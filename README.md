# micro-service

Servidor de micro-servicios usando framework Hapi.js en Node.js, para el manejo de informaciond e antenas Wi-Fi en los edificions pertenecientes a la UNSL.

Este proyecto fue generado a partir del artículo creado en [Zankuda - Microservicios con NodeJS](https://www.zankuda.com/2015/08/01/Micro-Servicios-Con-NodeJS/).

##Versión 1.0
Creación de servicio de ejemplo con pruebas automáticas completas.

##Versión 1.1
- Creación de archivos de configuración de ejecución para [PM2](https://github.com/Unitech/pm2).
- Agregada biblioteca [JSHint](http://jshint.com/) para validación de reglas de estilo de código fuente.
- Nuevas tareas definidas para [NPM](http://www.npmjs.org), de tal forma que genere reportes de resultados de pruebas automáticas.

Todo lo anterior es utilizado por [Jenkins](http://jenkins-ci.org/), para el ejemplo completo de *[DevOps](https://es.wikipedia.org/wiki/DevOps)*.
**Todo está explicado en el [tutorial de Zankuda.com](https://www.zankuda.com/2015/08/17/continuous-delivery-en-nodejs-con-jenkins-parte-1/)**

