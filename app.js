var pmx = require('pmx').init();
var config = require('./config');
var hapi = require('hapi');
var request = require('request');

const Wreck = require('wreck');

var server = new hapi.Server()



server.connection({ 
  host: config.server.host,
  port: config.server.port
});

server.register({
    register: require('h2o2')
}, function (err) {

    if (err) {
        console.log('Failed to load h2o2');
    }

    server.start(function (err) {

        console.log('Server started at: ' + server.info.uri);
    });
});


var routes = require('./routes')
routes.init(server, config)

server.start(function () {
  if (process.env.NODE_ENV != 'test') {
    console.log('Servidor ejecutándose en:', server.info.uri);
  }
});



if (module.parent) {
  if (process.env.NODE_ENV != 'test') {
    console.log("Llamada de ejecución como módulo")
  }
  module.exports = server
}

