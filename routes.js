
exports.init = function (server) {
  
  server.route({
      method: 'GET',
      path: '/stats/{ip}',
      config: {
          cors: {
              origin: ['*'],
              additionalHeaders: ['cache-control', 'x-requested-with']
          }
      },
      handler: {
          proxy: {
            mapUri: function(request, callback){

                callback(null,'http://'+request.params.ip+'/stats');
            },
            onResponse: function(err, res, request, reply, settings, ttl){
               console.log(res); 
               reply(res);
            },
            passThrough: true,
            xforward: true
          }
      }
  });

  server.route({
      method: 'GET',
      path: '/stats/{ip}/speedtest',
      config: {
          cors: {
              origin: ['*'],
              additionalHeaders: ['cache-control', 'x-requested-with']
          }
      },
      handler: {
          proxy: {
            mapUri: function(request, callback){

                callback(null,'http://'+request.params.ip+'/stats/speedtest');
            },
            onResponse: function(err, res, request, reply, settings, ttl){
               console.log(res); 
               reply(res);
            },
            passThrough: true,
            xforward: true
          }
      }
  });

  server.route({
    method: 'POST',
    path: '/bienvenido',
    handler: function (request, reply) {
      reply({
        statusCode: 0,
        mensaje: 'Bienvenida(o) ' + req.payload.name
      })
    }
  })

  server.route({
    method: 'POST',
    path: '/upload',
    config: {
      payload: {
        output: 'file',
        maxBytes: 1048576 * 10, // 10MB,
        parse: true,
        allow: 'multipart/form-data'
      }
    },
    handler: function (req, reply) {
      // Administración de flujo de archivos
      var fs = require('fs')
      // Funciones de manejo de rutas de arhcivos
      var path = require('path')
      // Generador de textos al azar.
      var randtoken = require('rand-token')
      if (req.payload.file) {
        var file = {
          name: randtoken.generate(60) + path.extname(req.payload.file.filename), // Se genera un texto de 60 caráteres y concatena la extensión del archivo
          contentType: req.payload.file.headers['content-type'],
          length: req.payload.file.bytes
        }
        fs.rename(req.payload.file.path, path.join(__dirname, '/upload/' + file.name), function(err) {
          if (err) {
            reply({
              statusCode: 100,
              mensaje: 'Error al guardar el archivo enviado...'
            })
          }
          else {
            reply({
              statusCode: 0,
              mensaje: 'Archivo recibido',
              file: file
            })
          }
        })
      }
    }
  })

  server.route({
      method: 'GET',
      path: '/buscarDNI/{dni}',
      config: {
          cors: {
              origin: ['*'],
              additionalHeaders: ['cache-control', 'x-requested-with']
          }
      },

      handler: {
          proxy: {
            mapUri: function(request, callback){

                callback(null,'http://int_dgti:mnbgfd876@ifx3wgu.unsl.edu.ar/apig3w/?d='+dni);
            },
            onResponse: function(err, res, request, reply, settings, ttl){
               console.log(res); 
               reply(res);
            },
            passThrough: true,
            xforward: true
          }
      }
  });

}
